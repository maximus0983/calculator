package com.example.calculator.service;

import com.example.calculator.common.Payment;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UtilServiceTest {
    private UtilService utilService = new UtilService();

    @Test
    void createPaymentsTest() {
    List<Payment> list = utilService.createPayments(4,1000000,new BigDecimal(123123), LocalDate.now(),13.);
    assertEquals(4,list.size());
    }

    @Test
    void totalAmountTest() {
        assertEquals(9.00,utilService.totalAmount(12,12,100).doubleValue());
    }
}