package com.example.calculator.common;

import java.math.BigDecimal;
import java.time.LocalDate;


public class Payment {
    private int count;
    private LocalDate date;
    private BigDecimal mainDebt;
    private BigDecimal procentDebt;
    private BigDecimal residual;
    private BigDecimal totalAmount;

    public Payment(int count, LocalDate date, BigDecimal mainDebt, BigDecimal procentDebt, BigDecimal residual, BigDecimal totalAmount) {
        this.count = count;
        this.date = date;
        this.mainDebt = mainDebt;
        this.procentDebt = procentDebt;
        this.residual = residual;
        this.totalAmount = totalAmount;
    }

    public Payment() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getMainDebt() {
        return mainDebt;
    }

    public void setMainDebt(BigDecimal mainDebt) {
        this.mainDebt = mainDebt;
    }

    public BigDecimal getProcentDebt() {
        return procentDebt;
    }

    public void setProcentDebt(BigDecimal procentDebt) {
        this.procentDebt = procentDebt;
    }

    public BigDecimal getResidual() {
        return residual;
    }

    public void setResidual(BigDecimal residual) {
        this.residual = residual;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "count=" + count +
                ", date=" + date +
                ", mainDebt=" + mainDebt +
                ", procentDebt=" + procentDebt +
                ", residual=" + residual +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
