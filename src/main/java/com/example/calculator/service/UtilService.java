package com.example.calculator.service;

import com.example.calculator.common.Payment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;

@Service
public class UtilService {
    private static final int YEAR_MONTHS = 12;
    private static final int DIVISOR = 100;

    public List<Payment> createPayments(long n, long sn, BigDecimal x, LocalDate date, double procent) {
        List<Payment> payments = new ArrayList<>();
        BigDecimal pn;// pn - платеж по процентам
        BigDecimal mainDebt;// mainDebt - платеж основного долга
        BigDecimal residual; // остаток основного долга
        for (int i = 0; i < n; i++) {
            pn = BigDecimal.valueOf(sn * procent / YEAR_MONTHS / DIVISOR).setScale(2, RoundingMode.HALF_UP);
            mainDebt = x.subtract(pn).setScale(2, RoundingMode.HALF_UP);
            residual = BigDecimal.valueOf(sn - mainDebt.doubleValue());
            payments.add(new Payment(i + 1, date, mainDebt, pn, residual, x));
            if (sn < x.doubleValue()) {
                payments.get(i).setResidual(new BigDecimal(0));
                payments.get(i).setTotalAmount(mainDebt);
            }
            sn = residual.longValue();
            date = date.plusMonths(1);
        }
        return payments;
    }

    public BigDecimal totalAmount(double p, long n, long sn) {
        double absoluteProcent = p / 12 / 100;
        return (BigDecimal.valueOf(absoluteProcent).divide((BigDecimal.valueOf(1 - pow(1 + absoluteProcent, -n))),
                RoundingMode.HALF_UP))
                .multiply(BigDecimal.valueOf(sn)).setScale(2, RoundingMode.HALF_UP);
    }
}
