package com.example.calculator.controller;

import com.example.calculator.common.Payment;
import com.example.calculator.service.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Controller
public class RestController {
    @Value("${procent}")
    private double PROCENT;

    @Autowired
    private UtilService utilService;

    @GetMapping("/getPay/{sn}/{n}")// sn - сумма кредита, n - количество месяцев
    @ResponseBody
    public List<Payment> getPayments(@PathVariable long sn, @PathVariable long n) {
        LocalDate date = LocalDate.now().plusMonths(1);
        BigDecimal x;// х - ежемесячный платеж
        x = utilService.totalAmount(PROCENT, n, sn);
        List<Payment> payments = utilService.createPayments(n, sn, x, date, PROCENT);
        return payments;
    }

    @RequestMapping("/")
    public ModelAndView welcome() {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("procent", PROCENT);
        return modelAndView;
    }
}